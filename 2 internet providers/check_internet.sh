#!/bin/sh
HOST="1.1.1.1"
IP1="PRIMERY IP address"
GW1="PRIMERY gateway"
GW2="SECONDARY gateway"
# Lock file, create when internet change to secondary
LOCKFILE="/tmp/check_internet.lock"

# Log file
LOGFILE="/var/log/check_internet.log"
touch ${LOGFILE}

# Ping $HOST via primary gateway 
ping -I ${IP1} -c 3 -n -q ${HOST} > /dev/null

# Ping host is unreachable
if [ $? -ne "0" ]; then
        # If no Lock file
        if [ ! -f ${LOCKFILE} ]; then
                # Change default route in routing table 
                ip route del default
                ip route add default via ${GW2}
                # Create Lock file
                touch ${LOCKFILE}
                # Write log massage 
                echo `date +'%Y/%m/%d %H:%M:%S'` Internet connection changet to SECONDARY internet provider via ${GW2} >> ${LOGFILE}
        fi
# Ping host is reachable 
else
        # If Lock file is present
        if [ -f ${LOCKFILE} ]; then
                # Change default route in routing table
                ip route del default
                ip route add default via ${GW1}
                # Delete Lock file
                rm -f ${LOCKFILE}
                # Write log massage
                echo `date +'%Y/%m/%d %H:%M:%S'` Internet connection changed to PRIMERY internet provider via ${GW1} >> ${LOGFILE}
        fi
fi
